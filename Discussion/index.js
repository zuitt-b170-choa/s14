// one line comment (ctrl+slash)
/*
multiline comment (ctrl+shift+slash)
*/

console.log("Hello Batch 170"); //this is called a statement

/*
	Javascript - we can see the messages/log in the console

	Browser Console - are part of the browsers which will allow us to see/log
	the messages, data, or information from the programming language they
	are easily accessed in the dev tools.

	Statements instructions, expressionss that we add to the programming
	language to communicate with the computers.

		usually ending with a semicolon (;) However, JS has implemented a way to automatically add semicolons at the end of the line. (an automation done by JS)

	Syntax
		set of rules that describes how statements should be made or constructed.
		lines/blocks of codes must follow a certain set of rules for it to work properly.
*/

console.log("Gabriel Kester Choa");

/*
	create three console logs to display your favorite food
*/

console.log("Mcdo Shake Shake Fries");
console.log("Sinigang");
console.log("Mango");

let food = ["Mcdo", "Sinigang", "Mango"];
let food1 = "Adobo";
console.log(food[0],food[1],food[2]);

/*
	Variables are a way to store information or data within the JS
	to create a variable, we first declare the name of the variable with either let/const keyword:
		let nameofVar
		then, we can initialize(define) the variable with a value or data.

			let nameofVar = "= - Assignment operator" <valueOfVariable>
*/

console.log("My favorite food is: "+food[0]);

console.log("My favorite food is: " +food[0] +" and " +food[1]);

console.log(food[2]);

let food3;
console.log(food3);
/*
	we can create variables without values, but the variables' content would log undefined.
*/


food3 = "chickenjoy";
/*
	we can update the content of a variable by reassigning the value using an assignment operator(=);

	assignment operator(=) lets us (re)assign to a variable
*/


console.log(food3);

/*
	we cannot create another variable with the same name. it will result into an error.
		let food1 = "flat tops";

		console.log(food1);
*/

// const keyword

const pi=3.1416;

console.log(pi);

let pi = "pizza";

console.log(pi);

/*
mini activity
Let Key
*/

food1 = "choclem"