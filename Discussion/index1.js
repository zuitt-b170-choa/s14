// one line comment (ctrl + slash)
/*
	multiline comment (ctrl + shift + slash)
*/

console.log("Hello Batch 170!");

/*
	Javascript- we can see the messages/log in the console.

	Browser Consoles are part of the browsers which will allow us to see/log messages, data or information from the programming language
	
		they are easily accessed in the dev tools

	Statements
		instructions, expressions that we add to the programming language to communicate with the computers.

		usually ending with a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of the the line.
	
	Syntax
		set of rules that describes how statements should be made/constructed

		lines/blocks of codes must follow a certain set of rules for it to work properly
*/

console.log("Gabriel")
/*
	create three console logs to display your favorite food
		console.log("<yourFavoriteFood>")
*/
/*console.log("Adobo")
console.log("Adobo")
console.log("Adobo")*/

let food1 = "Fries"

console.log(food1);

/*
	Variables are way to store information or data within the JS.

	to create a variable, we first declare the name of the variable with either let/const keyword:

		let nameOfVariable

	Then, we can initialize(define) the variable with a value or data
		
		let nameOfVariable = <valueOfVariable>
*/

console.log("My favorite food is: " + food1);

let food2 = "Sinigang";

console.log("My favorite food is: " + food1 + " and " + food2);

let food3;

/*
	we can create variables without values, but the variables' content would log undefined.
*/
console.log(food3);

food3 = "Mango";
/*
	we can update the content of a variable by reassigning the value using an assignment operator (=);

	assignment operator (=) lets us (re)assign values to a variable
*/
console.log(food3)

food1 = "adobo";
food2 =	"fish";

console.log(food1);
console.log(food2);

const sunriseDirection = "East";
const sunsetDirection = "West";

/*Guideline

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declarataion of the variable name; and Initialization of the initial value of the variable thought the use of assignment operator (=).

	3. A 'let' variable can be declared without initialization. However, the value of the variable will be undefined until it is reassigned with a value.
	
	4. Not defined cs undefined. Not defined error means that the variable is used but not declared. Undefined results from a variable that is used but not initialized.

	5. We can use const keyword to create variables. Constant variables cannot be declared without initialization. Const variable values cannot be reassigned.

	6. When creating variable name, start with small caps, this is because of avoiding conflict with JS that is named with capital letters. (camelcase)

	7. If the variable would need two words, the naming convention is to use camelCase. Do not add spaces for the variables with words as names.
*/

// Data Types

/*
	string
		are data types which are alphanumerical text. They could be a name, phrase, or even sentences. We can create strindata types with a single quot ('') or with a double quote (""). The text inside the quotation marks will be displayed as it is.

*/

console.log("Sample String Data");

/*
numeric data types
	are data types which are numeric. numeric data types are displayed when numbers are not placed in the quotation marks. if there is mathematical operation needed to be done, numeric data type should be used instead of string data type.
*/

console.log(0123456789);
console.log(1+1);

/*
	mini activity
		using string data type
		display in the console your:
			name (first and last)
			birthday
			province where you live

		Using numerical data type
			display in the console your
				high score in the last game you played.
				your favorite number

*/

console.log("Gabriel Choa");
console.log("August 9, 1998");
console.log("Metro Manila");

console.log(30);
console.log(8);
console.log(3);

