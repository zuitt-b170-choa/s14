// JS Functions
/*
	Functions are used to create reusable commands/statements that prevent the dev from typing a bunch codes. In field, a big number of lines of codes is the normal output; using functions would save a lot of time and effort in typing the codes that will be used multiple times.
*/
function printStar(){
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();
printStar();
printStar();

function sayHello(name){
	/*
		functions can also use "parameters", these parameters can be defined and a part of the command inside the function. when called, parameters can be replaced with the target value of the developer.

		SYNTAX
		function functionName(parameters){
			command/statement
		}
	*/
	console.log("Hello" + name)
}

sayHello(" GabChoa");

/*function alertPrint(){
	alert("Hello");
	console.log("Hello");
}

alertPrint();*/

//Function that accepts two numbers and prints the sum

function add(x,y){
	let sum = x+y;
	console.log(sum);

}


add(1,2);
//add (832, 440, 123); if the number of parameters defined exceeds the needed, the excess would be ignored by JS.

//Three Parameters
// displayed the firstName, lastName, age

function printBio(firstName, lastName, age){
	console.log("Hello " + firstName + " " + lastName + " " + age);

	//using template literals
	/*
		declared by the use of backticks with dollarsign and curly braces with the parameters inside the braces. 1 curly brace is for 1 paramter. 1:1 ratio. the displayed data in the console will be defined parameter instead of the text inside the curly braces.
	*/
	//backticks ``
	console.log(`Hello ${firstName} ${lastName} ${age}`);
}

printBio("Gab", "Choa", 18);

function createFullName(fname, mname, lname){
	// 'return' specifies the value to be given back by the function once it is finishing executing the function. the value can be given to a variable. that's why we also need to log the variable in the console outside the function.
	return `${fname} ${mname} ${lname}`
}

let fullName = createFullName("Juan", "Dela", "Cruz");

console.log(fullName);


