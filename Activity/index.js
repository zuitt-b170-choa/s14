console.log("Hello World");

let fname = "John";
let lname = "Smith";
var age = 30;
let hobbies = ["Biking", "Mountain climbing", "Swimming"];
let workAdd = ["32", "Washington", "Lincoln", "Nebraska"];

console.log("First Name: " + fname);
console.log("Last Name: " + lname);
console.log("Age: " +age);
console.log("Hobbies: ");
console.log(hobbies);
console.log(`Work Address: `);
console.log("{houseNumber: %o, street: %o, city: %o, state: %o}",workAdd[0], workAdd[1], workAdd[2], workAdd[3]);


function printUserInfo(){
	console.log(`${fname} ${lname} is ${age} years of age.`);
	console.log(`This was printed inside of the function.`);
	console.log(hobbies);
	console.log(`This was printed inside of the function.`);
	console.log("{houseNumber: %o, street: %o, city: %o, state: %o}",workAdd[0], workAdd[1], workAdd[2], workAdd[3]);

	console.log()
}

printUserInfo();

function returnFunction(isMarried){
	return `The value of isMarried is: ${isMarried}`;
}

let isMarried = returnFunction("true");
console.log(isMarried);